#!/usr/bin/env python3

import argparse
import curses
import signal
import shutil
import math

columns, rows = shutil.get_terminal_size()

#D51
D51STR1 = "      ====        ________                ___________ "
D51STR2 = "  _D _|  |_______/        \\__I_I_____===__|_________| "
D51STR3 = "   |(_)---  |   H\\________/ |   |        =|___ ___|   "
D51STR4 = "   /     |  |   H  |  |     |   |         ||_| |_||   "
D51STR5 = "  |      |  |   H  |__--------------------| [___] |   "
D51STR6 = "  | ________|___H__/__|_____/[][]~\\_______|       |   "
D51STR7 = "  |/ |   |-----------I_____I [][] []  D   |=======|__ "
D51WHL11 = "__/ =| o |=-~~\\  /~~\\  /~~\\  /~~\\ ____Y___________|__ "
D51WHL12 = " |/-=|___|=    ||    ||    ||    |_____/~\\___/        "
D51WHL13 = "  \\_/      \\O=====O=====O=====O_/      \\_/            "
D51WHL21 = "__/ =| o |=-~~\\  /~~\\  /~~\\  /~~\\ ____Y___________|__ "
D51WHL22 = " |/-=|___|=O=====O=====O=====O   |_____/~\\___/        "
D51WHL23 = "  \\_/      \\__/  \\__/  \\__/  \\__/      \\_/            "
D51WHL31 = "__/ =| o |=-O=====O=====O=====O \\ ____Y___________|__ "
D51WHL32 = " |/-=|___|=    ||    ||    ||    |_____/~\\___/        "
D51WHL33 = "  \\_/      \\__/  \\__/  \\__/  \\__/      \\_/            "
D51WHL41 = "__/ =| o |=-~O=====O=====O=====O\\ ____Y___________|__ "
D51WHL42 = " |/-=|___|=    ||    ||    ||    |_____/~\\___/        "
D51WHL43 = "  \\_/      \\__/  \\__/  \\__/  \\__/      \\_/            "
D51WHL51 = "__/ =| o |=-~~\\  /~~\\  /~~\\  /~~\\ ____Y___________|__ "
D51WHL52 = " |/-=|___|=   O=====O=====O=====O|_____/~\\___/        "
D51WHL53 = "  \\_/      \\__/  \\__/  \\__/  \\__/      \\_/            "
D51WHL61 = "__/ =| o |=-~~\\  /~~\\  /~~\\  /~~\\ ____Y___________|__ "
D51WHL62 = " |/-=|___|=    ||    ||    ||    |_____/~\\___/        "
D51WHL63 = "  \\_/      \\_O=====O=====O=====O/      \\_/            "
D51DEL = "                                                      "
COAL01 = "                              "
COAL02 = "                              "
COAL03 = "    _________________         "
COAL04 = "   _|                \\_____A  "
COAL05 = " =|                        |  "
COAL06 = " -|                        |  "
COAL07 = "__|________________________|_ "
COAL08 = "|__________________________|_ "
COAL09 = "   |_D__D__D_|  |_D__D__D_|   "
COAL10 = "    \\_/   \\_/    \\_/   \\_/    "
COALDEL = "                              "


def my_mvaddstr(screen, row, col, move_string):
    track_col = col
    if col < 0:
        track_col = 0
        if abs(col) > len(move_string):
            return curses.ERR
    for iteration in range(0,len(move_string)-1):
#        if curses.ERR == screen.move(row, track_col) or curses.ERR == screen.addch(move_string[iteration]):
        screen.addch(move_string[iteration])
#        if curses.ERR == screen.addch(row, track_col, move_string[iteration]):
#            return curses.ERR
        track_col+=1
    return curses.OK

def add_sl(x):
    print("add_sl: BEGIN")
    print("add_sl: END")

def add_D51(screen, col):
    d51_height = 10
    d51_funnel = 7
    d51_length = 83
    d51_patterns = 6

    d51 = [[D51STR1, D51STR2, D51STR3, D51STR4, D51STR5, D51STR6, D51STR7, D51WHL11, D51WHL12, D51WHL13, D51DEL],
           [D51STR1, D51STR2, D51STR3, D51STR4, D51STR5, D51STR6, D51STR7, D51WHL21, D51WHL22, D51WHL23, D51DEL],
           [D51STR1, D51STR2, D51STR3, D51STR4, D51STR5, D51STR6, D51STR7, D51WHL31, D51WHL32, D51WHL33, D51DEL],
           [D51STR1, D51STR2, D51STR3, D51STR4, D51STR5, D51STR6, D51STR7, D51WHL41, D51WHL42, D51WHL43, D51DEL],
           [D51STR1, D51STR2, D51STR3, D51STR4, D51STR5, D51STR6, D51STR7, D51WHL51, D51WHL52, D51WHL53, D51DEL],
           [D51STR1, D51STR2, D51STR3, D51STR4, D51STR5, D51STR6, D51STR7, D51WHL61, D51WHL62, D51WHL63, D51DEL]]
    coal = [COAL01, COAL02, COAL03, COAL04, COAL05, COAL06, COAL07, COAL08, COAL09, COAL10, COALDEL]

    if col < 0-d51_length:
        return curses.ERR
    row = math.ceil(rows / 2) - 5
    move_row = 0

    if args.fly:
        row = (col / 7) + rows - (cols / 7) - d51_height
        move_row = 1

    for iteration in range(1,d51_height):
        my_mvaddstr(screen, row + iteration, col, d51[(d51_length + col) % d51_patterns][iteration])
        my_mvaddstr(screen, row + iteration + move_row, col + 53, coal[iteration])

    if args.accident:
        add_man(row + 2, col + 43)
        add_man(row + 2, col + 47)
    add_smoke(row-1,col+d51_funnel)
    return

def add_C51(x):
    print("add_C51: BEGIN")
    print("add_C51: END")

def add_man(y, x):
    pass

def add_smoke(y, x):
    pass

def main():
    print("MAIN: BEGIN")

    screen = curses.initscr()
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    curses.noecho()
    curses.curs_set(0)
    screen.nodelay(1)
    screen.leaveok(1)
    screen.scrollok(0)

    for col in range(columns-1, 0, -1):
        if True == args.logo:
            try:
                add_sl(col)
            except:
                break
        elif True == args.c51:
            try:
                add_C51(col)
            except:
                break
        else:
            try:
                add_D51(screen, col)
            except:
                break
        screen.getch()
        screen.refresh()
        curses.napms(40)
        #curses.napms(40000)
    screen.move(rows-1, 0)
    curses.endwin()
    print("MAIN: END")


if __name__ == "__main__":
    print("BEGIN")
    # Parse arguments
    parser = argparse.ArgumentParser(description ='SL (Steam Locomotive) runs across your terminal when you type "sl" as you meant to type "ls". It is just a joke command, and not useful at all.')
    parser.add_argument('-a', dest = 'accident', action = 'store_true', help = 'Accident')
    parser.add_argument('-F', dest = 'fly', action = 'store_true', help = 'Fly')
    parser.add_argument('-l', dest = 'logo', action = 'store_true', help = 'Logo')
    parser.add_argument('-c', dest = 'c51', action = 'store_true', help = 'C51')
    args = parser.parse_args()

    #print("ARGS:\n accident=>{0}\n fly=>{1}\n logo=>{2}\n c51=>{3}".format(args.accident, args.fly, args.logo, args.c51))
    
    main()
    print("END")
